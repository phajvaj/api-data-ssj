FROM node:14-alpine
LABEL maintainer="Banjong Kittisawangwong<saciw.doi@gmail.com>"
WORKDIR /home/api
RUN apk update && \
    apk upgrade && \
    apk add --no-cache \
    alpine-sdk git curl py-pip \
    build-base libtool autoconf \
    automake gzip g++ \
    make tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone
COPY . .

RUN npm i && npm run build:dist

EXPOSE 5500

CMD ["node", "./dist/app.js"]
