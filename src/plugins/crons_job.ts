// const http = require('request');
var fastifyPlugin = require('fastify-plugin')
import cron = require('node-cron');
import {PersonModel} from "../models/person";

// import * as LineAPI from 'line-api';

const _model = new PersonModel();

function fastifyCronJob(fastify, opts, next) {
  const db = fastify.db;
  try {
    //api alert pm to line notify
    cron.schedule(opts.job_call, async function(){
      try {
        await _model.list(db, 'xxx');
      } catch (error) {
        console.log(error.message);
      }
    });

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = fastifyPlugin(fastifyCronJob, '>=0.10.0')
