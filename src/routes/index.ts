/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import moment = require('moment');
import * as HttpStatus from 'http-status-codes';

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.get('/', async (req: fastify.Request, reply: fastify.Reply) => {
    reply.code(200).send({ message: 'API HDC SSJ v.1.65.08.08-1148' })
  });

  fastify.get('/info', async (req: fastify.Request, reply: fastify.Reply) => {
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'year').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'PHINGOSOFT',
      description: 'for access iot api',
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'y' });

    console.log(token);
    try {
      reply.code(HttpStatus.OK).send({ ok: true })
    } catch (error) {
      console.log(error);
      reply.code(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
