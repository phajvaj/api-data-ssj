/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import {PersonModel} from "../models/person";

const _model = new PersonModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.post('/hospcode', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const data = req.body;
    try {
      const vs: any = await _model.readByhosp(db, data);
      if (vs.length == 0) {
        reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
