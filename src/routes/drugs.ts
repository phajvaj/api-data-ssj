/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import {DrugModel} from "../models/drug";

const _model = new DrugModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.post('/allergy', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.listAllergy(db, cid);
      vs = vs[0];// get result for data
      if (vs.length == 0) {
        reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.post('/warfarin', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.DrugByCheck(db, cid, process.env.WR_FIELD, process.env.WR_ICODE, process.env.WR_DAYS);
      vs = vs[0];// get result for data
      if (vs.length == 0) {
        reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  fastify.post('/streptokinase', { preHandler: [fastify.authenticate] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const cid = req.body.cid;
    try {
      let vs: any = await _model.DrugByCheck(db, cid, process.env.SK_FIELD, process.env.SK_ICODE, process.env.SK_DAYS);
      vs = vs[0];// get result for data
      if (vs.length == 0) {
        reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: 'ไม่พบข้อมูล' });
        return ;
      }

      reply.status(HttpStatus.OK).send({ ok: true, rows: vs })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
