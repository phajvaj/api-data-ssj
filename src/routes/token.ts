/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as Random from 'random-js';

import { TokenModel } from '../models/token';
import moment = require('moment');

const tokenModel = new TokenModel();

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.post('/', async (req: fastify.Request, reply: fastify.Reply) => {
    const { hosxp } = req.body;
    if (hosxp !== process.env.HOSP_CODE) {
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'hosp code not found!' })
    }
    const createdDate = moment().format('YYYY-MM-DD HH:mm:ss');
    const expiredDate = moment().add(process.env.EXPIRE_TOKEN, 'year').format('YYYY-MM-DD HH:mm:ss');
    const token = fastify.jwt.sign({
      issue: 'API',
      description: 'for access api',
      HOSxP: process.env.HOSP_CODE,
    }, { expiresIn: process.env.EXPIRE_TOKEN + 'y' });

    const data: any = {
      token: token,
      created: createdDate,
      expired: expiredDate
    };

    try {
      reply.status(HttpStatus.OK).send({ ok: true , rows: data })
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.INTERNAL_SERVER_ERROR).send({ ok: false, error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR) })
    }
  });

  next();

};

module.exports = router;
