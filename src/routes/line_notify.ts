/// <reference path="../../typings.d.ts" />

import * as Knex from 'knex';
import * as fastify from 'fastify';
import * as HttpStatus from 'http-status-codes';
import * as LineAPI from 'line-api';

import { UserModel } from '../models/user';

const request = require('request');
const userModel = new UserModel();
import * as crypto from 'crypto';

const notify = new LineAPI.Notify({
  token: process.env.LINE_TOKEN,
});

const router = (fastify, { }, next) => {

  var db: Knex = fastify.db;

  fastify.get('/authorize', { preHandler: [fastify.authenticate, fastify.verifyMember] }, async (req: fastify.Request, reply: fastify.Reply) => {
    try {
      const line:any = {
        method: 'GET',
        url: `https://notify-bot.line.me/oauth/authorize`,
        qs: {
          response_type: 'code',
          client_id: process.env.LINE_CLIENT_ID,
          redirect_uri: process.env.LINE_CLIENT_CALLBACK,
          //redirect_uri: `http://localhost:3080/#/user`,
          scope: 'notify',
          state: crypto.createHash('md5').update(`${process.env.LINE_CLIENT_ID}:-:${req.user.userId}`).digest('hex'),
        },
      };
      reply.status(HttpStatus.OK).send({ ok: true, data: line});

    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message })
    }

  });

  fastify.post('/token', { preHandler: [fastify.authenticate, fastify.verifyMember] }, async (req: fastify.Request, reply: fastify.Reply) => {
    const line_code = req.body.code;
    const line_state = req.body.state;

    try {
      let psw = crypto.createHash('md5').update(`${process.env.LINE_CLIENT_ID}:-:${req.user.userId}`).digest('hex');

      if(line_state !== psw)
        reply.status(HttpStatus.OK).send({ ok: false, error: 'State not validity'});

      request({
        method: 'POST',
        uri: `https://notify-bot.line.me/oauth/token`,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
          grant_type: 'authorization_code',
          code: line_code,
          client_id: process.env.LINE_CLIENT_ID,
          redirect_uri: process.env.LINE_CLIENT_CALLBACK,
          client_secret: process.env.LINE_CLIENT_SECRET,
        }
      }, async (err, res, body) => {
        if(err){
          console.log(err);
          reply.status(HttpStatus.OK).send({ ok: false, error: err});
        } else {
          const js = await JSON.parse(body);
          const update:any = {
            is_notify: 'Y',
            line_token: js.access_token,
          };
          await userModel.update(db, req.user.userId, update);
          reply.status(HttpStatus.OK).send({ ok: true, data: js});
        }
      });

    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message })
    }

  });

  fastify.post('/revoke', { preHandler: [fastify.authenticate, fastify.verifyMember] }, async (req: fastify.Request, reply: fastify.Reply) => {
    try {

      const user = await userModel.read(db, req.user.userId);
      const rs = user[0];
      console.log(rs);

      if(rs.is_notify==='Y'){
        request({
          method: 'POST',
          uri: `https://notify-api.line.me/api/revoke`,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${rs.line_token}`,
          },
        }, async (err, res, body) => {
          if(err){
            console.log(err);
            reply.status(HttpStatus.OK).send({ ok: false, error: err});
          } else {
            const js = await JSON.parse(body);
            console.log(js);
            if(js.message==='ok'){
              const update:any = {
                is_notify: 'N',
                line_token: null,
              };
              await userModel.update(db, req.user.userId, update);
              reply.status(HttpStatus.OK).send({ ok: true, data: js});
            }else{
              reply.status(HttpStatus.OK).send({ ok: false, error: 'not logout'});
            }
          }
        });
      }

    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.UNAUTHORIZED).send({ ok: false, statusCode: HttpStatus.INTERNAL_SERVER_ERROR, error: error.message })
    }

  });
  next();
}

module.exports = router;
