/// <reference path="../typings.d.ts" />

import path = require('path');
import * as HttpStatus from 'http-status-codes';
import * as fastify from 'fastify';
import * as multer  from 'fastify-multer';
import { Server, IncomingMessage, ServerResponse } from 'http';

import helmet = require('fastify-helmet');

import * as moment from 'moment';
import router from './router';

const serveStatic = require('serve-static');
const mysqlssh = require('knex-mysqlssh-dialect/mysqlssh');

require('dotenv').config({ path: path.join(__dirname, '../config') });

const app: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({
  logger: {
    level: 'error',
    prettyPrint: true
  },
  bodyLimit: 5 * 1048576,
});

app.register(require('fastify-formbody'));
app.register(require('fastify-cors'), {});
app.register(require('fastify-no-icon'));
app.register(multer.contentParser);

app.register(require('fastify-static'), {
  root: path.join(__dirname, '../public'),
  //prefix: '/public/', // optional: default '/'
});

app.register(
  helmet,
  { hidePoweredBy: { setTo: 'PHP 5.2.0' } }
);

app.register(require('fastify-rate-limit'), {
  global : false,
  max: +process.env.MAX_CONNECTION_PER_MINUTE || 3000,
  timeWindow: '1.5 minute',
  skipOnError: true,
  cache: 10000,
});

app.use(serveStatic(path.join(__dirname, '../public')));

app.register(require('fastify-jwt'), {
  secret: process.env.SECRET_KEY
});

var templateDir = path.join(__dirname, '../templates');
app.register(require('point-of-view'), {
  engine: {
    ejs: require('ejs')
  },
  templates: templateDir
});

app.decorate("authenticate", async (request, reply) => {
  var token = null;

  if (request.headers.authorization && request.headers.authorization.split(' ')[0] === 'Bearer') {
    token = request.headers.authorization.split(' ')[1];
  } else if (request.query && request.query.token) {
    token = request.query.token;
  } else {
    token = request.body.token;
  }

  try {
    const decoded = await request.jwtVerify(token);
  } catch (err) {
    reply.status(HttpStatus.UNAUTHORIZED).send({
      statusCode: HttpStatus.UNAUTHORIZED,
      error: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED),
      message: '401 UNAUTHORIZED!'
    })
  }
});

let dbConfig:any = {
  client: 'mysql',
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    port: +process.env.DB_PORT,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    typeCast: function (field, next) {
      if (field.type == 'DATETIME') {
        return moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
      }
      if (field.type == 'DATE') {
        return moment(field.string()).format('YYYY-MM-DD');
      }
      return next();
    }
  },
  pool: {
    min: 0,
    max: 7,
    afterCreate: (conn, done) => {
      conn.query('SET NAMES utf8', (err) => {
        done(err, conn);
      });
    }
  },
  debug: false,
};

if (process.env.JUMP_HOST) {
  dbConfig.client = mysqlssh;
  dbConfig.connection.tunnelConfig = {
    src: {
      host: 'localhost',
      port: +process.env.DB_PORT,
    },
    dst: {
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
    },
    jmp: {
      host: process.env.JUMP_HOST,
      port: +process.env.JUMP_PORT,
      auth: {
        user: process.env.JUMP_USER,
        pass: process.env.JUMP_PASSWORD,
      },
    },
  };
}

// Database
app.register(require('./plugins/db'), {
  config: dbConfig,
  connectionName: 'db'
});

// MQTT
/*app.register(require('./plugins/mqtt'), {
  host: process.env.MQTT_SERVER,
  port: process.env.MQTT_PORT,
  username: process.env.MQTT_USER,
  password: process.env.MQTT_PASSWORD
});*/

// Cron job
if (process.env.JUMP_HOST) {
  app.register(require('./plugins/crons_job'), {
    job_call: '*/15 * * * * *',
  });
}

app.decorate('verifyAdmin', function (request, reply, done) {
  if (request.user.userType === 'ADMIN') {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.decorate('verifyMember', function (request, reply, done) {
  if (request.user.userType === 'MEMBER') {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.decorate('verifyAPI', function (request, reply, done) {
  if (request.user.issue === 'API' && request.user.HOSxP === process.env.HOSP_CODE) {
    done();
  } else {
    reply.status(HttpStatus.UNAUTHORIZED).send({ statusCode: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
  }
});

app.register(router);

const port = +process.env.API_PORT || 3000;
const host = '0.0.0.0';

app.listen(port, host, (err) => {
  if (err) throw err;
  console.log(app.server.address());
});
