import { FastifyInstance } from 'fastify';

export default async function router(fastify: FastifyInstance) {
    fastify.register(require('./routes/index'), { prefix: '/', logger: true });
    fastify.register(require('./routes/login'), { prefix: '/login', logger: true });
    fastify.register(require('./routes/token'), { prefix: '/token', logger: true });
    fastify.register(require('./routes/person'), { prefix: '/person', logger: true });
}
