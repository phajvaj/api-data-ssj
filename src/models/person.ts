import * as knex from 'knex';

export class PersonModel {

  tableName: string = 'person';
  pk = 'cid';

  list(db: knex, id: any = null) {
    let sql = db(this.tableName)
        .select();

    if(id)
      sql.where(this.pk, id);

    return sql;
  }

  readByhosp(db: knex, data: any) {
    const sql = db(this.tableName + ' as p').select('h.hoscode','h.hosname')
        .join('chospital as h', 'p.hospcode', 'h.hoscode')
        .where(`p.${this.pk}`, data.cid)
        .andWhereRaw(`p.hospcode<>'${data.hospcode}'`);

    if(data.htype === 'pcu'){
      /*sql.andWhereRaw(`((h.distcode='${data.distcode}' and h.provcode='${data.provcode}' and h.hostype='07')
      or (h.hostype IN('06', '05') and h.provcode='${data.provcode}'))`);*/
      sql.andWhereRaw(`(h.hostype IN('06', '05', '07') and h.provcode='${data.provcode}')`);
    }else {
      sql.andWhereRaw(`h.hostype IN('06', '07', '05', '11')`);
    }
    sql.groupBy('p.hospcode');
    return sql;
  }
}
/*
select h.hoscode,h.hosname from person as p
inner join chospital as h on(p.hospcode=h.hospcode)
where p.cid='xxx' and h.hostype in('06','07')
group by p.hospcode
 */
