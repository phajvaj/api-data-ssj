import * as knex from 'knex';
import * as util from 'util';

export class DrugModel {

  listAllergy(db: knex, cid: any) {
    return db.raw(`SELECT a.agent,a.agent_code24,a.symptom,a.note,a.begin_date,a.report_date FROM opd_allergy as a
      INNER JOIN patient as p ON(a.hn=p.hn) WHERE p.cid='${cid}'`);
  }

  DrugByvn(db: knex, vn: any) {
    return db.raw(`select o.item_no,concat(s.name,' ',s.strength,' ',s.units) as name,o.qty,d.shortlist,o.sp_use 
    from opitemrece o 
    inner join ovst as v on(o.vn=v.vn or (o.an=v.an and o.item_type='H'))
    left outer join s_drugitems s on(s.icode=o.icode)
    left outer join drugusage d on (d.drugusage=o.drugusage)
    left outer join sp_use u on (u.sp_use = o.sp_use)
    left outer join drugitems i on (i.icode=o.icode) where v.vn='${vn}' and o.icode like '1%'  order by o.item_no`);
  }

  DrugByCheck(db: knex, cid: any, field: any, did: any, days: any) {
    return db.raw(`SELECT IF(o.vn IS NOT NULL, 'op', 'ip') as service,d.\`name\` as drug_name,o.vstdate,o.qty,d.units,d.strength,DATEDIFF(CURDATE(), o.vstdate)  as dt
    FROM opitemrece as o
    INNER JOIN drugitems as d ON(o.icode=d.icode) 
    INNER JOIN patient as p ON(o.hn=p.hn) 
    WHERE d.${field} LIKE '${did}'
    AND p.cid='${cid}'
    AND o.qty > 0
    AND DATEDIFF(CURDATE(), o.vstdate) <= ${days}
    ORDER BY o.vstdate DESC LIMIT 1`);
  }
}
