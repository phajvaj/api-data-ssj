import * as knex from 'knex';

export class TokenModel {

  tableName: string = 'tokens';

  list(db: knex) {
    return db(this.tableName);
  }

  find(db: knex, id: any) {
    return db(this.tableName).where('id', id);
  }

  save(db: knex, data: any) {
    return db(this.tableName).insert(data);
  }

  remove(db: knex, id: any) {
    return db(this.tableName)
      .where('id', id)
      .del();
  }

}
